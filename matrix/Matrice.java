import java.util.*;

public class Matrice {
  public static void main(String[] args) {
    Scanner scn = new Scanner(System.in);

    int r = 0;
    int c = 0;

    Random rnd = new Random();

    System.out.println(" scrivi il numero di righe della matrice ");
    r = scn.nextInt();


    System.out.println(" scrivi il numero di colonne della matrice ");
    c = scn.nextInt();


    System.out.println();

    if (r < 0 || c < 0) {
      System.err.println(" non puoi inserire numeri negativi ");
      System.exit(1);
    }

    MyMatrix m = new MyMatrix(r, c);

    for (int i = 0; i < r; i++) {
      for (int k= 0; k < c; k++) {
        m.setElemento(i, k, rnd.nextInt(100));
      }
    }

      m.showMatrix(m);
      m.isSimmetrica(m);

    System.out.println(" scrivi il numero di righe della seconda matrice ");
    r = scn.nextInt();

    System.out.println(" scrivi il numero di colonne della seconda matrice ");
    c = scn.nextInt();

    System.out.println();

    if (r < 0 || c < 0) {
      System.err.println(" non puoi inserire numeri negativi ");
      System.exit(1);
    }

    MyMatrix n = new MyMatrix(r, c);

    for (int i = 0; i < r; i++) {
      for (int k = 0; k < c; k++) {
        n.setElemento(i, k, rnd.nextInt(100));
      }
    }

    n.showMatrix(n);
    n.isSimmetrica(n);

    try {
      m.moltiplicazione(m, n);
    }catch (IllegalArgumentException e) {
      System.err.println("le due matrici non sono conformabili");
    }


    try {
      n.somma(m, n);
    }catch (IllegalArgumentException e) {
      System.err.println("le due matrici non hanno le stesse dimensioni");
    }


  }
}
