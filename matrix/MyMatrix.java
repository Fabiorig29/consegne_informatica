public class MyMatrix {
  public int R;
  public int C;
  public int[][] matrice;

  public MyMatrix(){}

  public MyMatrix(int R, int C) {
    this.R = R;
    this.C = C;
    matrice = new int[R][C];
  }

  public int getNr() {
    return R;
  }

  public int getNc() {
    return C;
  }

  public void setElemento(int i, int j, int val){
    matrice[i][j] = val;
   }

   public static void showMatrix(MyMatrix m) {
     for(int i = 0; i < m.R; i++) {
       for(int j=0; j < m.C; j++) {
         System.out.print(m.matrice[i][j] + " ");
       }
       System.out.println();
     }
     System.out.println();
   }

   public boolean isSimmetrica(MyMatrix s) {
     boolean b = true;
     if (s.R != s.C) {
       b = false;
     }else {
       for (int i = 0; i < s.R - 1; i++) {
         for (int j = 1; j < s.R; j++) {
           if (matrice[i][j] != matrice[j][i]) {
             b = false;
           }
         }
       }
     }

     if (b == false) {
       System.out.println(" non simmetrica \n ");
     }else {
       System.out.println(" simmetrica \n ");
     }
     return b;
   }

   public static MyMatrix moltiplicazione (MyMatrix a, MyMatrix b) {
     MyMatrix c = new MyMatrix(a.R, b.C);
     if (a.C != b.R) {
       throw new IllegalArgumentException();
     }else {
        for (int i = 0; i < a.R; i++) {
          for (int k = 0; k < b.C; k++) {
            for (int j = 0; j < a.C; j++) {
              c.matrice[i][k] += a.matrice[i][j] * b.matrice[j][k];
            }
          }
        }
         }
         System.out.println(" prodotto ");
         showMatrix(c);
         return c;
       }

       public static MyMatrix somma (MyMatrix a, MyMatrix b) {
         MyMatrix c = new MyMatrix(a.R, a.C);
         if (a.R != b.R || a.C != b.C) {
           throw new IllegalArgumentException();
         }else {
           for (int i = 0; i < b.R; i++) {
             for (int j = 0; j < b.C; j++) {
               c.matrice[i][j] = a.matrice[i][j] += b.matrice[i][j];
             }
           }
         }
         System.out.println("somma ");
         showMatrix(c);
         return c;
       }

     }
