public class Es16 {
  public static void main(String[] args) {

    int[] a = new int[args.length + 1];

    for (int i = 0; i < args.length; i++) {
              a[i] = Integer.parseInt(args[i]);
    }

    int n = 0, c=0;
    for (int i = 0; i < a.length - 1; i++) {
      for (int j = i + 1; j < a.length; j++) {
        if (a[i] > a[j]) {
          n = a[i];
          a[i] = a[j];
          a[j] = n;
        }
      }
    }

    for (int i = 0; i < a.length - 1; i++) {
      c = 1;
      for (int j = i + 1; j < a.length; j++) {
        if (a[i] == a[j])
          c++;
      }

      if (a[i] > 0) {

          if (a[i + 1] == a[i]) {
            a[i + 1] = -1;
          }
        if (a[i-1] != a[i]) {
          System.out.print(a[i] + " " + " ");
          for (int j = 0; j < c; j++) {
            System.out.print("*");
          }
          System.out.println();
        }

      }

      if (a[i] == - 1) {
        a[i] = a[i - 1];
      }

    }

    if (a[a.length - 1] != a[a.length - 2]) {
      System.out.print(a[a.length - 1] + " *");
    }
  }
}
